if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
 // some code..
}else
{
//Now include js files
}


var html = document.documentElement;
var body = document.body;
var sec1 = document.querySelector("#sec1");
var plus = sec1.clientHeight
var sec1Bg = document.querySelector("#sec1-bg");
var plusBg = sec1Bg.clientHeight

if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
	var scroller = {
		target: document.querySelector("#scroll-container"),
		ease: 1, // <= scroll speed
		endY: 0,
		y: 0,
		resizeRequest: 1,
		scrollRequest: 0,
	};
}else
{
	var scroller = {
		target: document.querySelector("#scroll-container"),
		ease: 0.1, // <= scroll speed
		endY: 0,
		y: 0,
		resizeRequest: 1,
		scrollRequest: 0,
	};
}

var requestId = null;

TweenLite.set(scroller.target, {
	rotation: 0.01,
	force3D: true
});

window.addEventListener("load", onLoad);

function onLoad() {
	"use strict";
	updateScroller();
	window.focus();
	window.addEventListener("resize", onResize);
	document.addEventListener("scroll", onScroll);
}

function updateScroller() {
	"use strict";
	var resized = scroller.resizeRequest > 0;

	if (resized) {
		var height = scroller.target.clientHeight;
		body.style.height = height + plus - plusBg - 250 + "px";
		scroller.resizeRequest = 0;
	}

	var scrollY = window.pageYOffset || html.scrollTop || body.scrollTop || 0;

	scroller.endY = scrollY;
	scroller.y += (scrollY - scroller.y) * scroller.ease;

	if (Math.abs(scrollY - scroller.y) < 0.05 || resized) {
		scroller.y = scrollY;
		scroller.scrollRequest = 0;
	}

	TweenLite.set(scroller.target, {
		y: -scroller.y
	});

	requestId = scroller.scrollRequest > 0 ? requestAnimationFrame(updateScroller) : null;
}

function onScroll() {
	"use strict";
	scroller.scrollRequest++;
	if (!requestId) {
		requestId = requestAnimationFrame(updateScroller);
	}
}

function onResize() {
	"use strict";
	scroller.resizeRequest++;
	if (!requestId) {
		requestId = requestAnimationFrame(updateScroller);
	}
}
