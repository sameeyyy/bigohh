/*jshint esversion: 6*/
var tl = new TimelineMax();
var tl2 = new TimelineMax();
var tl3 = new TimelineMax();
var tl4 = new TimelineMax();
var tl5 = new TimelineMax();
var tl6 = new TimelineMax();
var tl7 = new TimelineMax();
var tl8 = new TimelineMax();
var tl9 = new TimelineMax();

var hline = CSSRulePlugin.getRule(".section2 .h-line:after");
var tl10 = new TimelineMax();

const controller = new ScrollMagic.Controller();


tl.to(".mid-hd-anim h1", 0.5, {scale: 0.5,});
tl.to(".mid-hd-anim h1", 0.3, {top: "10px", opacity: 0});
tl2.from(".navani1", 0.1, {top: "0px", opacity: 1});
tl2.to(".navani1", 0.1, {top: "-80px"});
tl2.from(".navani2", 0.1, {delay: 0.1, top: "0px"});
tl2.to(".navani2", 0.1, {top: "-80px"});
tl2.from(".navani3", 0.1, {delay: 0.2, top: "0px"});
tl2.to(".navani3", 0.1, {top: "-80px"});
tl2.from(".navani4", 0.1, {delay: 0.3, top: "0px"});
tl2.to(".navani4", 0.1, {top: "-80px"});
// tl3.from("#parallax", 1.5, {scale: 0.7, opacity: 0});
// tl3.to("#parallax", 1.5, {scale: 1, opacity: 1});
tl4.to("#parallax", 0.5, {opacity: "0"});
tl5.to(".arrows", 0.5, {opacity: "0"});
tl4.from(".section2", 0.5, {opacity: 0});
tl4.to(".section2", 0.5, {marginTop: "-250px", opacity: 1});
tl7.to(".ledge", 1, {rotate: 27, opacity: 1});
tl7.from(".rw1, .rw2", 0.5, {marginTop: "-60px", marginLeft: "-200px", opacity: 0});
tl7.to(".rw1, .rw2", 0.5, {marginTop: "0px", marginLeft: "0px", opacity: 1});
tl8.to(".section1", 0.5, {background: "none"});
tl9.from(".navani5", 0.1, {delay: 2, top: "0px"});
tl9.to(".navani5", 0.1, {top: "-100px"});
tl9.from(".scrollTop", 1, {top: "20px", opacity: 0});
tl9.to(".scrollTop", 1, {top: "0px", opacity: 1});
tl10.to(hline, 1, {delay: 1, cssRule: {width: "24px"}});

const scene1 = new ScrollMagic.Scene({
		offset: 50
	})
	.setPin(".section1")
	.setTween(tl)
	.addTo(controller);

const scene2 = new ScrollMagic.Scene({
		offset: 50
	})
	.setTween(tl2)
	.addTo(controller);

// const scene3 = new ScrollMagic.Scene({
//   offset: 0
// })
//   .setPin("#parallax")
//   .setTween(tl3)
//     .addTo(controller);

const scene4 = new ScrollMagic.Scene({
		offset: 50
	})
	.setTween(tl4)
	.addTo(controller);

const scene5 = new ScrollMagic.Scene({
		offset: 50
	})
	.setTween(tl5)
	.addTo(controller);

const scene6 = new ScrollMagic.Scene({
		offset: 50
	})
	.setTween(tl6)
	.addTo(controller);

const scene7 = new ScrollMagic.Scene({
		triggerElement: '.section3'
	})
	.setTween(tl7)
	.addTo(controller);

const scene8 = new ScrollMagic.Scene({
		offset: 1
	})
	.setTween(tl8)
	.addTo(controller);

const scene9 = new ScrollMagic.Scene({
		offset: 50
	})
	.setTween(tl9)
	.addTo(controller);

const scene10 = new ScrollMagic.Scene({
		offset: 50
	})
	.setTween(tl10)
	.addTo(controller);
