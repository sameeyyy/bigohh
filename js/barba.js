/*jshint esversion: 6*/
/****************************************** SMOOTHSCROLL ******************************************/
function smoothScroll() {
	"use strict";
	   var html = document.documentElement;
	   var body = document.body;
	   var sec1 = document.querySelector("#sec1");
	   var plus = sec1.clientHeight;
	   var sec1Bg = document.querySelector("#sec1-bg");
	   var plusBg = sec1Bg.clientHeight;
	   
	   if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		   var scroller = {
			   target: document.querySelector("#scroll-container"),
			   ease: 1, // <= scroll speed
			   endY: 0,
			   y: 0,
			   resizeRequest: 1,
			   scrollRequest: 0,
		   };
	   }else
	   {
		   var scroller = {
			   target: document.querySelector("#scroll-container"),
			   ease: 0.1, // <= scroll speed
			   endY: 0,
			   y: 0,
			   resizeRequest: 1,
			   scrollRequest: 0,
		   };
	   }
	   
	   var requestId = null;
	   
	   TweenLite.set(scroller.target, {
		   rotation: 0.01,
		   force3D: true
	   });
	   
	   window.addEventListener("load", onLoad);
	   
	   function onLoad() {
		   updateScroller();
		   window.focus();
		   window.addEventListener("resize", onResize);
		   document.addEventListener("scroll", onScroll);
	   }
	   
	   function updateScroller() {
		   var resized = scroller.resizeRequest > 0;
	   
		   if (resized) {
			   var height = scroller.target.clientHeight;
			   body.style.height = height + plus - plusBg - 250 + "px";
			   scroller.resizeRequest = 0;
		   }
	   
		   var scrollY = window.pageYOffset || html.scrollTop || body.scrollTop || 0;
	   
		   scroller.endY = scrollY;
		   scroller.y += (scrollY - scroller.y) * scroller.ease;
	   
		   if (Math.abs(scrollY - scroller.y) < 0.05 || resized) {
			   scroller.y = scrollY;
			   scroller.scrollRequest = 0;
		   }
	   
		   TweenLite.set(scroller.target, {
			   y: -scroller.y
		   });
	   
		   requestId = scroller.scrollRequest > 0 ? requestAnimationFrame(updateScroller) : null;
	   }
	   
	   function onScroll() {
		   scroller.scrollRequest++;
		   if (!requestId) {
			   requestId = requestAnimationFrame(updateScroller);
		   }
	   }
	   
	   function onResize() {
		   scroller.resizeRequest++;
		   if (!requestId) {
			   requestId = requestAnimationFrame(updateScroller);
		   }
	   }
	
	
smoothScroll.onLoad = onLoad;
	   
}



/****************************************** PRELOAD ******************************************/
function preload() {
	"use strict";
	window.addEventListener('load', () => {
    setTimeout(function() {
        const preload = document.querySelector('.preload');
        preload.classList.add('preload-finish');
      }, 2000);
	});
}

/****************************************** CUSTOM JAVASCRIPT ******************************************/
function customjs () {

"use strict";
var dataBody = document.querySelector("body");

function introData() {
	dataBody.setAttribute('data-state', 'intro');
}
function whoData() {
	dataBody.setAttribute('data-state', 'who');
}
function whatData() {
	dataBody.setAttribute('data-state', 'what');
}
function workData() {
	dataBody.setAttribute('data-state', 'work');
}

customjs.introData = introData;
customjs.whoData = whoData;
customjs.whatData = whatData;
customjs.workData = workData;




 // ************************ PARALLAX ELEMENT **********************
 (function () {
	// Add event listener
	document.addEventListener("mousemove", parallax);
	const elem = document.querySelector("#parallax");
	// Magic happens here
	function parallax(e) {
		let _w = window.innerWidth / 2;
		let _h = window.innerHeight / 2;
		let _mouseX = e.clientX;
		let _mouseY = e.clientY;
		let _depth1 = `${50 - (_mouseX - _w) * 0.002}% ${50 - (_mouseY - _h) * 0.002}%`;
		let x = `${_depth1}`;
		elem.style.backgroundPosition = x;
	}

})();

// var lFollowX = 0,
//     lFollowY = 0,
//     x = 0,
//     y = 0,
//     friction = 1 / 30;

// function animate() {
//   x += (lFollowX - x) * friction;
//   y += (lFollowY - y) * friction;

//   translate = 'translate(' + x + 'px, ' + y + 'px) scale(1.1)';

//   $('#project-button, #project-button2, #project-button3, #project-button4').css({
//     '-webit-transform': translate,
//     '-moz-transform': translate,
//     'transform': translate
//   });

//   window.requestAnimationFrame(animate);
// }

// $(window).on('mousemove click', function(e) {

//   var lMouseX = Math.max(-100, Math.min(100, $(window).width() / 2 - e.clientX));
//   var lMouseY = Math.max(-100, Math.min(100, $(window).height() / 2 - e.clientY));
//   lFollowX = (80 * lMouseX) / 100; // 100 : 12 = lMouxeX : lFollow
//   lFollowY = (60 * lMouseY) / 100;

// });

// animate();




// ************************** SCROLL TO TOP **************************

var scrollTop = $(".scrollTop");


$(scrollTop).click(function () {
	$('html, body').animate({
		scrollTop: 0
	}, 200); // 800 is default
	return false;

});


// ************************** PAGE HEADINGS **************************
	


// ************************** MOBILE NAV **************************


//  $('.menu-icon').on('click', function(){
// 	$('.bar').toggleClass('bar--active')
// 	$('.overlay').toggleClass('overlay--active') 
//   })
   
//   $('.overlay').on('click', function(){
// 	if($('.overlay').hasClass('overlay--active')){
// 	  $('.overlay').removeClass('overlay--active')
// 	  $('.bar').removeClass('bar--active')
// 	}
//   })


function mobNav() {

	function openNav() {
		document.getElementById("nc1").style.transition = "3s";
		document.getElementById("myNav").style.width = "100%";
		document.getElementById("myNav").style.height = "100%";
		document.getElementById("nc1").style.opacity = "1";
		document.getElementById("myNav").style.opacity = "1";
		document.getElementById("myNav").style.pointerEvents = "initial";
	}

	function closeNav() {
		document.getElementById("nc1").style.transition = "0.5s";
		document.getElementById("myNav").style.transition = "0.5s";
		document.getElementById("nc1").style.opacity = "0";
		document.getElementById("myNav").style.width = "80%";
		document.getElementById("myNav").style.height = "80%";
		document.getElementById("myNav").style.opacity = "0";
		document.getElementById("myNav").style.pointerEvents = "none";
	}

	//$('#nav-icon2').click(openNav);
	//$('#nav-icon2').click(function(){
	//$(this).toggleClass('open');
	//if($(this).hasClass('open')) {
	//   $('#nav-icon2').click(closeNav);
	//}
	//else {
	//   $('#nav-icon2').click(openNav);
	//}
	//});


//	$('#nav-icon2').click(openNav);
	$('#nav-icon2, .overlay a').click(function() {
	if($('#nav-icon2').hasClass('open')) {
		setTimeout( function() {        
          $('#nav-icon2').removeClass('open');
		  closeNav();
       }, 1 );
		
	}
	else {
		setTimeout( function() {  
			$('#nav-icon2').addClass('open');
			openNav();
		}, 1 );
	}
	});
	
//	$('.overlay a').click(function() {
//		$('#nav-icon2').removeClass('open');
//		closeNav();
//	});

//	$('.overlay a').click(function() {
//		$('#nav-icon2').removeClass('open');
//		closeNav();
//	});
}
customjs.mobNav = mobNav;
//  $(document).ready(function(){
// 	$('#nav-icon2').click(openNav)
// 	$('#nav-icon2').click(function(){
// 		$(this).toggleClass('open');
// 	});
// 	$('#opened').click(closeNav);
// });

// ************************** CONTACT OVERLAY **************************

var contOpen = $(".navani5");
var contClose = $(".closebtn2");


$(contOpen).click(function() {
	document.getElementById("oc2").style.transition = "3s";
	document.getElementById("contact-overlay").style.height = "95%";
	document.getElementById("oc2").style.opacity = "1";
});

$(contClose).click(function() {
	document.getElementById("oc2").style.transition = "0.5s";
	document.getElementById("contact-overlay").style.transition = "0.5s";
	document.getElementById("oc2").style.opacity = "0";
	document.getElementById("contact-overlay").style.height = "0%";
});

// ************************** FOOTER YEAR **************************

var dteNow = new Date();
var intYear = dteNow.getFullYear();

$('.cr-text').each(function () {
	var text = $(this).text();
	$(this).text(text.replace('CurrentYear', intYear));
});



/************************ WORK ANIMATION *************************/

//  if ($("body").data("state") === "work") {
   function viIn() {
	   $('.vi-work').addClass('show');
   }
  
   function viOut() {
	   $('.vi-work').removeClass('show');
   }
  
   $('.vi').hover(viIn, viOut);
  
   /* FOR V2 */
  
   function awIn() {
	   $('.aw-work').addClass('show');
   }
  
   function awOut() {
	   $('.aw-work').removeClass('show');
   }
  
   $('.aw').hover(awIn, awOut);
// }

/************************ COUNTUP ANIMATION *************************/

function countUp() {
var a = 0;
$(window).scroll(function () {
	var oTop = $('#counter').offset().top - window.innerHeight;
	if (a === 0 && $(window).scrollTop() > oTop) {
		$('.counter-value').each(function () {
			var $this = $(this),
				countTo = $this.attr('data-count');
			$({
				countNum: $this.text()
			}).animate({
					countNum: countTo
				},

				{

					duration: 3000,
					easing: 'swing',
					step: function () {
						$this.text(Math.floor(this.countNum));
					},
					complete: function () {
						$this.text(this.countNum);
						//alert('finished');
					}

				});
		});
		a = 1;
	}

});
}
customjs.countUp= countUp;

/************************ PRELOADER *************************/

// window.addEventListener('load', () => {
//	 "use strict";
//    setTimeout(function() {
//        const preload = document.querySelector('.preload');
//        preload.classList.add('preload-finish');
//      }, 2000);
//});

/************************** CURSOR **************************/

// document.getElementsByTagName("body")[0].addEventListener("mousemove", function(n) {
// 	t.style.left = n.clientX + "px", 
// 	t.style.top = n.clientY + "px", 
// 	e.style.left = n.clientX + "px", 
// 	e.style.top = n.clientY + "px", 
// 	i.style.left = n.clientX + "px", 
// 	i.style.top = n.clientY + "px"
// });
// var t = document.getElementById("cursor"),
// 	e = document.getElementById("cursor2"),
// 	i = document.getElementById("cursor3");
// function n(t) {
// 	e.classList.add("hover"), i.classList.add("hover")
// }
// function s(t) {
// 	e.classList.remove("hover"), i.classList.remove("hover")
// }
// s();
// for (var r = document.querySelectorAll(".hover-target"), a = r.length - 1; a >= 0; a--) {
// 	o(r[a])
// }
// function o(t) {
// 	t.addEventListener("mouseover", n), t.addEventListener("mouseout", s)
// }
}

/****************************************** SCROLLMAGIC ******************************************/
function smagic() {
"use strict";
var tl = new TimelineMax();
var tl2 = new TimelineMax();
//var tl3 = new $.TimelineMax();
var tl4 = new TimelineMax();
var tl5 = new TimelineMax();
var tl6 = new TimelineMax();
var tl7 = new TimelineMax();
var tl8 = new TimelineMax();
var tl9 = new TimelineMax();
var hline = CSSRulePlugin.getRule(".section2 .h-line:after");
var tl10 = new TimelineMax();
var hline3 = CSSRulePlugin.getRule(".section3 .h-line3:after");
var tl11 = new TimelineMax();
var hline4 = CSSRulePlugin.getRule(".section4 .h-line4:after");
var tl12 = new TimelineMax();
var hline5 = CSSRulePlugin.getRule(".section5 .h-line5:after");
var tl13 = new TimelineMax();
var tl14 = new TimelineMax();
var tl15 = new TimelineMax();
var tl16 = new TimelineMax();
var tl17 = new TimelineMax();
var tl18 = new TimelineMax();
var tl19 = new TimelineMax();
var tl20 = new TimelineMax();
var tl21 = new TimelineMax();
var tl22 = new TimelineMax();
var tl23 = new TimelineMax();
var tl24 = new TimelineMax();
var tl25 = new TimelineMax();
var tl26 = new TimelineMax();
var tl27 = new TimelineMax();

const controller = new ScrollMagic.Controller();


// tl.to(".mid-hd-anim h1", 0.5, {scale: 0.5, opacity: 1});
tl.to(".arrows", 0.1, {display: "none"});
tl.to(".mid-hd-sty", 1, {top: "20%",opacity: 0});
tl.to("#parallax", 1, {opacity: "0"});
tl.to("#parallax", 0.05, {display: "none"});
tl.from(".section2", 0.001, {opacity: 0});
tl.to(".section2", 0.5, {marginTop: "-250px",opacity: 1});
tl2.from(".navani1", 0.1, {top: "0px",opacity: 1});
tl2.to(".navani1", 0.1, {top: "-80px"});
tl2.from(".navani2", 0.1, {delay: 0.1,top: "0px"});
tl2.to(".navani2", 0.1, {top: "-80px"});
tl2.from(".navani3", 0.1, {delay: 0.2,top: "0px"});
tl2.to(".navani3", 0.1, {top: "-80px"});
tl2.from(".navani4", 0.1, {delay: 0.3,top: "0px"});
tl2.to(".navani4", 0.1, {top: "-80px"});
//tl3.from("#parallax", 3, {delay: 0.5, scale: 1.1});
//tl3.to("#parallax", 1.5, {scale: 1});
tl4.to(".sec1-bg, .sec1-cont", 1, {delay: 1.5, background: "none", pointerEvents: "none"});
//if ($("body").data("state") === "who") {
	tl5.from(".founder-sec-main", 1, {marginTop: "200px",opacity: 0});
	tl5.to(".founder-sec-main", 0.1, {marginTop: "0px",opacity: 1});
	tl6.from(".founder-sec-main-2", 1, {marginTop: "200px",opacity: 0});
	tl6.to(".founder-sec-main-2", 0.1, {marginTop: "0px",opacity: 1});
//}
//if ($("body").data("state") === "intro" || ($("body").data("state") === "who") || ($("body").data("state") === "what")) {
	tl7.to(".ledge", 1.5, {rotate: 27});
//}
//if ($("body").data("state") === "intro" || ($("body").data("state") === "who")) {
	tl7.from(".rw1, .rw2", 0.5, {marginTop: "-60px", marginLeft: "-200px", opacity: 0});
	tl7.to(".rw1, .rw2", 0.5, {marginTop: "0px", marginLeft: "0px", opacity: 1 });
//}
tl8.from(".section3, .section4, .section5, .section6, .footer", 0.5, {delay: 1, opacity: 0});
tl8.to(".section3, .section4, .section5, .section6, .footer", 0.5, {opacity: 1});
tl9.from(".navani5, .clicknav", 0.1, {delay: 2, top: "0px"});
tl9.to(".navani5, .clicknav", 0.1, {top: "-100px"});
tl9.from(".scrollTop", 1, {opacity: 0, display: "none"});
tl9.to(".scrollTop", 1, {opacity: 1, display: "initial"});
tl10.from(hline, 0.1, {cssRule: {width: "0px"}});
tl10.to(hline, 1, {delay: 2.5, cssRule: {width: "24px"}});
tl11.to(hline3, 1, {delay: 1, cssRule: {width: "24px"}});
tl12.to(hline4, 1, {cssRule: {width: "24px"}});
tl13.to(hline5, 1, {cssRule: {width: "24px"}});
//if ($("body").data("state") === "what") {
	tl14.from(".client:nth-child(odd)", 1, {opacity: 0});
	tl14.from(".client:nth-child(even)", 1, {opacity: 0});
	tl14.to(".client:nth-child(odd)", 1, {opacity: 1});
	tl14.to(".client:nth-child(even)", 1, {opacity: 1});
//}
//if ($("body").data("state") === "what") {
	tl15.from(".awards1", 1, {opacity: 0});
	tl15.from(".awards2", 1, {opacity: 0});
	tl15.from(".awards3", 1, {opacity: 0});
	tl15.to(".awards1", 0, {opacity: 1});
	tl15.to(".awards2", 0, {opacity: 1});
	tl15.to(".awards3", 0, {opacity: 1});
//}

//if ($("body").data("state") === "work") {
	tl16.to(".section3 .work-sub-hd, .section3 .work-sub-hd2", 1, {top: "100px", opacity: 1});
	tl16.to(".section3 .work-sub-hd, .section3 .work-sub-hd2", 2, {left: "35%"});
	tl17.to(".project-work1", 20, {scale: 1.2});
	tl18.to(".section4 .work-sub-hd, .section4 .work-sub-hd2", 1, {top: "100px", opacity: 1});
	tl18.to(".section4 .work-sub-hd, .section4 .work-sub-hd2", 2, {left: "45%"});
	tl19.to(".project-work2", 20, {scale: 1.2});
	tl20.to(".section5 .work-sub-hd, .section5 .work-sub-hd2", 1, {top: "100px", opacity: 1});
	tl20.to(".section5 .work-sub-hd, .section5 .work-sub-hd2", 2, {left: "45%"});
	tl21.to(".project-work3", 20, {scale: 1.2});
	tl22.to(".section6 .work-sub-hd, .section6 .work-sub-hd2", 1, {top: "100px", opacity: 1});
	tl22.to(".section6 .work-sub-hd, .section6 .work-sub-hd2", 2, {left: "45%"});
	tl23.to(".project-work4", 20, {scale: 1.2});
	/*For Mobile */
	tl24.to(".section3 .work-sub-hd, .section3 .work-sub-hd2", 2, {top: "25%", opacity: 1});
	tl25.to(".section4 .work-sub-hd, .section4 .work-sub-hd2", 2, {top: "30%", opacity: 1});
	tl26.to(".section5 .work-sub-hd, .section5 .work-sub-hd2", 2, {top: "30%", opacity: 1});
	tl27.to(".section6 .work-sub-hd, .section6 .work-sub-hd2", 2, {top: "30%", opacity: 1});
//}

const scene1 = new ScrollMagic.Scene({
		offset: 0.1
	})
	.setTween(tl)
	.addTo(controller);

const scene2 = new ScrollMagic.Scene({
		offset: 0.1
	})
	.setTween(tl2)
	.addTo(controller);

//const scene3 = new ScrollMagic.Scene({
//  offset: 0
//})
//  .setPin("#parallax")
//  .setTween(tl3)
//    .addTo(controller);

const scene4 = new ScrollMagic.Scene({
		offset: 0.1
	})
	.setTween(tl4)
	.addTo(controller);

const scene5 = new ScrollMagic.Scene({
		triggerElement: '.section4'
	})
	.setTween(tl5)
	.addTo(controller);

const scene6 = new ScrollMagic.Scene({
		triggerElement: '.section5'
	})
	.setTween(tl6)
	.addTo(controller);

const scene7 = new ScrollMagic.Scene({
		triggerElement: '.section3'
	})
	.setTween(tl7)
	.addTo(controller);

const scene8 = new ScrollMagic.Scene({
		triggerElement: ".section2"
	})
	.setTween(tl8)
	.addTo(controller);

const scene9 = new ScrollMagic.Scene({
		offset: 50
	})
	.setTween(tl9)
	.addTo(controller);

const scene10 = new ScrollMagic.Scene({
		offset: 50
	})
	.setTween(tl10)
	.addTo(controller);

const scene11 = new ScrollMagic.Scene({
		triggerElement: ".section3"
	})
	.setTween(tl11)
	.addTo(controller);

const scene12 = new ScrollMagic.Scene({
		triggerElement: ".section4"
	})
	.setTween(tl12)
	.addTo(controller);

const scene13 = new ScrollMagic.Scene({
		triggerElement: ".section5"
	})
	.setTween(tl13)
	.addTo(controller);

const scene14 = new ScrollMagic.Scene({
		triggerElement: ".section4"
	})
	.setTween(tl14)
	.addTo(controller);

const scene15 = new ScrollMagic.Scene({
		triggerElement: ".section5"
	})
	.setTween(tl15)
	.addTo(controller);

const scene16 = new ScrollMagic.Scene({
		triggerElement: ".section3"
	})
	.setTween(tl16)
	.addTo(controller);

const scene17 = new ScrollMagic.Scene({
		triggerElement: ".section3"
	})
	.setTween(tl17)
	.addTo(controller);

const scene18 = new ScrollMagic.Scene({
		triggerElement: ".section4"
	})
	.setTween(tl18)
	.addTo(controller);

const scene19 = new ScrollMagic.Scene({
		triggerElement: ".section4"
	})
	.setTween(tl19)
	.addTo(controller);

const scene20 = new ScrollMagic.Scene({
		triggerElement: ".section5"
	})
	.setTween(tl20)
	.addTo(controller);

const scene21 = new ScrollMagic.Scene({
		triggerElement: ".section5"
	})
	.setTween(tl21)
	.addTo(controller);

const scene22 = new ScrollMagic.Scene({
		triggerElement: ".section6"
	})
	.setTween(tl22)
	.addTo(controller);

const scene23 = new ScrollMagic.Scene({
		triggerElement: ".section6"
	})
	.setTween(tl23)
	.addTo(controller);

/* For Mobile */
const scene24 = new ScrollMagic.Scene({
		triggerElement: ".section3"
	})
	.setTween(tl24)
	.addTo(controller);

const scene25 = new ScrollMagic.Scene({
		triggerElement: ".section4"
	})
	.setTween(tl25)
	.addTo(controller);

const scene26 = new ScrollMagic.Scene({
		triggerElement: ".section5"
	})
	.setTween(tl26)
	.addTo(controller);

const scene27 = new ScrollMagic.Scene({
		triggerElement: ".section6"
	})
	.setTween(tl27)
	.addTo(controller);



function sizeAll() {
	var w = window.innerWidth;

	if (w < 1025) {
		scene16.enabled(false);
		scene18.enabled(false);
		scene20.enabled(false);
		scene22.enabled(false);

	} else {
		scene16.enabled(true);
		scene18.enabled(true);
		scene20.enabled(true);
		scene22.enabled(true);
	}

	if (w >= 1025) {
		scene24.enabled(false);
		scene25.enabled(false);
		scene26.enabled(false);
		scene27.enabled(false);

	} else {
		scene24.enabled(true);
		scene25.enabled(true);
		scene26.enabled(true);
		scene27.enabled(true);
	}

}

	$(window).resize(sizeAll);
	sizeAll();
	
}
smagic();
customjs();
preload();
smoothScroll();
// customjs.introData();
customjs.mobNav();
smoothScroll.onLoad();
smoothScroll();
smoothScroll();
customjs.countUp();


// /************************** BARBA **************************/

// var allPages = function () {
// "use strict";
// 	customjs();
// 	preload();
// };

// var intro = Barba.BaseView.extend({
// 	namespace: 'intro',
// 	onEnterCompleted: function () {
// 		"use strict";
// 		allPages();
// 		smoothScroll();
// 		smagic();
// 		customjs.introData();
// 		customjs.mobNav();
// 		smoothScroll.onLoad();
// 	}
// });
// var who = Barba.BaseView.extend({
// 	namespace: 'who',
// 	onEnter: function() {
// 		"use strict";
// 		allPages();
// 		customjs.whoData();
//     },
// 	onEnterCompleted: function () {
// 		"use strict";
// 		allPages();
// 		smoothScroll();
// 		smagic();
// 		customjs.mobNav();
// 		smoothScroll.onLoad();
// 	}
// });
// var what = Barba.BaseView.extend({
// 	namespace: 'what',
// 	onEnter: function() {
// 		"use strict";
// 		allPages();
// 		customjs.whatData();
//     },
// 	onEnterCompleted: function () {
// 		"use strict";
// 		allPages();
// 		smoothScroll();
// 		smagic();
// 		customjs.countUp();
// 		customjs.mobNav();
// 		smoothScroll.onLoad();
// 	}
// });
// var work = Barba.BaseView.extend({
// 	namespace: 'work',
// 	onEnter: function() {
// 		"use strict";
// 		allPages();
// 		customjs.workData();
//     },
// 	onEnterCompleted: function () {
// 		"use strict";
// 		allPages();
// 		smoothScroll();
// 		smagic();
// 		customjs.mobNav();
// 		smoothScroll.onLoad();
// 	}
// });

// intro.init();
// who.init();
// what.init();
// work.init();


// var FadeTransition = Barba.BaseTransition.extend({
// 	start: function() {
// 		"use strict";
// 		Promise.all([this.newContainerLoading, this.fadeOut()]).then(
// 			this.fadeIn.bind(this)
// 		);
// 	},
// 	fadeOut: function() {
// 		"use strict";
// 		var body = document.body;
// 		body.classList.toggle("disable");
// 		this.oldContainer.classList.add("slide-out");
// 	},
	
// 	fadeIn: function() {
// 		"use strict";
// 		var body = document.body;
// 		this.newContainer.classList.add("slide-in");
		
// 		var that = this;
		
// 		this.newContainer.addEventListener("animationend",function handler() {
// 		  that.newContainer.classList.remove("slide-in");
//           that.done();
// 		  that.newContainer.removeEventListener("animationend",handler);
// 		  that.newContainer.removeAttribute("style");
// 			setTimeout( function() {  
// 				body.classList.remove("disable");
// 			}, 0.1);
// 		});
// 	}
// });

// Barba.Pjax.getTransition = function() {
// 	"use strict";
// 	return FadeTransition;
// };

// Barba.Prefetch.init();
// Barba.Pjax.start();

//console.log = function() {};