/*jshint esversion: 6*/
var tl = new TimelineMax();
var tl2 = new TimelineMax();
var tl3 = new TimelineMax();
var tl4 = new TimelineMax();
var tl5 = new TimelineMax();
var tl6 = new TimelineMax();
var tl7 = new TimelineMax();
var tl8 = new TimelineMax();
var tl9 = new TimelineMax();
var hline = CSSRulePlugin.getRule(".section2 .h-line:after");
var tl10 = new TimelineMax();
var hline3 = CSSRulePlugin.getRule(".section3 .h-line3:after");
var tl11 = new TimelineMax();
var hline4 = CSSRulePlugin.getRule(".section4 .h-line4:after");
var tl12 = new TimelineMax();
var hline5 = CSSRulePlugin.getRule(".section5 .h-line5:after");
var tl13 = new TimelineMax();
var tl14 = new TimelineMax();
var tl15 = new TimelineMax();
var tl16 = new TimelineMax();
var tl17 = new TimelineMax();
var tl18 = new TimelineMax();
var tl19 = new TimelineMax();
var tl20 = new TimelineMax();
var tl21 = new TimelineMax();
var tl22 = new TimelineMax();
var tl23 = new TimelineMax();
var tl24 = new TimelineMax();
var tl25 = new TimelineMax();
var tl26 = new TimelineMax();
var tl27 = new TimelineMax();

const controller = new ScrollMagic.Controller();


// tl.to(".mid-hd-anim h1", 0.5, {scale: 0.5, opacity: 1});
tl.to(".arrows", 0.1, {display: "none"});
tl.to(".mid-hd-sty", 1, {top: "20%",opacity: 0});
tl.to("#parallax", 1, {opacity: "0"});
tl.to("#parallax", 0.05, {display: "none"});
tl.from(".section2", 0.001, {opacity: 0});
tl.to(".section2", 0.5, {marginTop: "-250px",opacity: 1});
tl2.from(".navani1", 0.1, {top: "0px",opacity: 1});
tl2.to(".navani1", 0.1, {top: "-80px"});
tl2.from(".navani2", 0.1, {delay: 0.1,top: "0px"});
tl2.to(".navani2", 0.1, {top: "-80px"});
tl2.from(".navani3", 0.1, {delay: 0.2,top: "0px"});
tl2.to(".navani3", 0.1, {top: "-80px"});
tl2.from(".navani4", 0.1, {delay: 0.3,top: "0px"});
tl2.to(".navani4", 0.1, {top: "-80px"});
tl3.from("#parallax", 3, {delay: 2, scale: 1.1});
tl3.to("#parallax", 1.5, {scale: 1});
tl4.to(".sec1-bg, .sec1-cont", 1, {delay: 1.5, background: "none", pointerEvents: "none"});
if ($("body").data("state") === "who") {
	tl5.from(".founder-sec-main", 1, {marginTop: "200px",opacity: 0});
	tl5.to(".founder-sec-main", 0.1, {marginTop: "0px",opacity: 1});
	tl6.from(".founder-sec-main-2", 1, {marginTop: "200px",opacity: 0});
	tl6.to(".founder-sec-main-2", 0.1, {marginTop: "0px",opacity: 1});
}
if ($("body").data("state") === "intro" || ($("body").data("state") === "who") || ($("body").data("state") === "what")) {
	tl7.to(".ledge", 1.5, {rotate: 27});
}
if ($("body").data("state") === "intro" || ($("body").data("state") === "who")) {
	tl7.from(".rw1, .rw2", 0.5, {marginTop: "-60px", marginLeft: "-200px", opacity: 0});
	tl7.to(".rw1, .rw2", 0.5, {marginTop: "0px", marginLeft: "0px", opacity: 1 });
}
tl8.from(".section3, .section4, .section5, .section6, .footer", 0.5, {delay: 1, opacity: 0});
tl8.to(".section3, .section4, .section5, .section6, .footer", 0.5, {opacity: 1});
tl9.from(".navani5, .clicknav", 0.1, {delay: 2, top: "0px"});
tl9.to(".navani5, .clicknav", 0.1, {top: "-100px"});
tl9.from(".scrollTop", 1, {opacity: 0, display: "none"});
tl9.to(".scrollTop", 1, {opacity: 1, display: "initial"});
tl10.from(hline, 0.1, {cssRule: {width: "0px"}});
tl10.to(hline, 1, {delay: 2.5, cssRule: {width: "24px"}});
tl11.to(hline3, 1, {cssRule: {width: "24px"}});
tl12.to(hline4, 1, {cssRule: {width: "24px"}});
tl13.to(hline5, 1, {cssRule: {width: "24px"}});
if ($("body").data("state") === "what") {
	tl14.from(".client:nth-child(odd)", 1, {opacity: 0});
	tl14.from(".client:nth-child(even)", 1, {opacity: 0});
	tl14.to(".client:nth-child(odd)", 1, {opacity: 1});
	tl14.to(".client:nth-child(even)", 1, {opacity: 1});
}
if ($("body").data("state") === "what") {
	tl15.from(".awards1", 1, {opacity: 0});
	tl15.from(".awards2", 1, {opacity: 0});
	tl15.from(".awards3", 1, {opacity: 0});
	tl15.to(".awards1", 0, {opacity: 1});
	tl15.to(".awards2", 0, {opacity: 1});
	tl15.to(".awards3", 0, {opacity: 1});
}
if ($("body").data("state") === "work") {
	tl16.to(".section3 .work-sub-hd, .section3 .work-sub-hd2", 1, {top: "100px", opacity: 1});
	tl16.to(".section3 .work-sub-hd, .section3 .work-sub-hd2", 2, {left: "35%"});
	tl17.to(".project-work1", 20, {scale: 1.2});
	tl18.to(".section4 .work-sub-hd, .section4 .work-sub-hd2", 1, {top: "100px", opacity: 1});
	tl18.to(".section4 .work-sub-hd, .section4 .work-sub-hd2", 2, {left: "45%"});
	tl19.to(".project-work2", 20, {scale: 1.2});
	tl20.to(".section5 .work-sub-hd, .section5 .work-sub-hd2", 1, {top: "100px", opacity: 1});
	tl20.to(".section5 .work-sub-hd, .section5 .work-sub-hd2", 2, {left: "45%"});
	tl21.to(".project-work3", 20, {scale: 1.2});
	tl22.to(".section6 .work-sub-hd, .section6 .work-sub-hd2", 1, {top: "100px", opacity: 1});
	tl22.to(".section6 .work-sub-hd, .section6 .work-sub-hd2", 2, {left: "45%"});
	tl23.to(".project-work4", 20, {scale: 1.2});
	/*For Mobile */
	tl24.to(".section3 .work-sub-hd, .section3 .work-sub-hd2", 2, {top: "25%", opacity: 1});
	tl25.to(".section4 .work-sub-hd, .section4 .work-sub-hd2", 2, {top: "30%", opacity: 1});
	tl26.to(".section5 .work-sub-hd, .section5 .work-sub-hd2", 2, {top: "30%", opacity: 1});
	tl27.to(".section6 .work-sub-hd, .section6 .work-sub-hd2", 2, {top: "30%", opacity: 1});
}

const scene1 = new ScrollMagic.Scene({
		offset: 0.1
	})
	.setTween(tl)
	.addTo(controller);

const scene2 = new ScrollMagic.Scene({
		offset: 0.1
	})
	.setTween(tl2)
	.addTo(controller);

const scene3 = new ScrollMagic.Scene({
  offset: 0
})
  .setPin("#parallax")
  .setTween(tl3)
    .addTo(controller);

const scene4 = new ScrollMagic.Scene({
		offset: 0.1
	})
	.setTween(tl4)
	.addTo(controller);

const scene5 = new ScrollMagic.Scene({
		triggerElement: '.section4'
	})
	.setTween(tl5)
	.addTo(controller);

const scene6 = new ScrollMagic.Scene({
		triggerElement: '.section5'
	})
	.setTween(tl6)
	.addTo(controller);

const scene7 = new ScrollMagic.Scene({
		triggerElement: '.section3'
	})
	.setTween(tl7)
	.addTo(controller);

const scene8 = new ScrollMagic.Scene({
		triggerElement: ".section2"
	})
	.setTween(tl8)
	.addTo(controller);

const scene9 = new ScrollMagic.Scene({
		offset: 50
	})
	.setTween(tl9)
	.addTo(controller);

const scene10 = new ScrollMagic.Scene({
		offset: 50
	})
	.setTween(tl10)
	.addTo(controller);

const scene11 = new ScrollMagic.Scene({
		triggerElement: ".section3"
	})
	.setTween(tl11)
	.addTo(controller);

const scene12 = new ScrollMagic.Scene({
		triggerElement: ".section4"
	})
	.setTween(tl12)
	.addTo(controller);

const scene13 = new ScrollMagic.Scene({
		triggerElement: ".section5"
	})
	.setTween(tl13)
	.addTo(controller);

const scene14 = new ScrollMagic.Scene({
		triggerElement: ".section4"
	})
	.setTween(tl14)
	.addTo(controller);

const scene15 = new ScrollMagic.Scene({
		triggerElement: ".section5"
	})
	.setTween(tl15)
	.addTo(controller);

const scene16 = new ScrollMagic.Scene({
		triggerElement: ".section3"
	})
	.setTween(tl16)
	.addTo(controller);

const scene17 = new ScrollMagic.Scene({
		triggerElement: ".section3"
	})
	.setTween(tl17)
	.addTo(controller);

const scene18 = new ScrollMagic.Scene({
		triggerElement: ".section4"
	})
	.setTween(tl18)
	.addTo(controller);

const scene19 = new ScrollMagic.Scene({
		triggerElement: ".section4"
	})
	.setTween(tl19)
	.addTo(controller);

const scene20 = new ScrollMagic.Scene({
		triggerElement: ".section5"
	})
	.setTween(tl20)
	.addTo(controller);

const scene21 = new ScrollMagic.Scene({
		triggerElement: ".section5"
	})
	.setTween(tl21)
	.addTo(controller);

const scene22 = new ScrollMagic.Scene({
		triggerElement: ".section6"
	})
	.setTween(tl22)
	.addTo(controller);

const scene23 = new ScrollMagic.Scene({
		triggerElement: ".section6"
	})
	.setTween(tl23)
	.addTo(controller);

/* For Mobile */
const scene24 = new ScrollMagic.Scene({
		triggerElement: ".section3"
	})
	.setTween(tl24)
	.addTo(controller);

const scene25 = new ScrollMagic.Scene({
		triggerElement: ".section4"
	})
	.setTween(tl25)
	.addTo(controller);

const scene26 = new ScrollMagic.Scene({
		triggerElement: ".section5"
	})
	.setTween(tl26)
	.addTo(controller);

const scene27 = new ScrollMagic.Scene({
		triggerElement: ".section6"
	})
	.setTween(tl27)
	.addTo(controller);



function sizeAll() {
	"use strict";
	var w = window.innerWidth;

	if (w < 1025) {
		scene16.enabled(false);
		scene18.enabled(false);
		scene20.enabled(false);
		scene22.enabled(false);

	} else {
		scene16.enabled(true);
		scene18.enabled(true);
		scene20.enabled(true);
		scene22.enabled(true);
	}

	if (w >= 1025) {
		scene24.enabled(false);
		scene25.enabled(false);
		scene26.enabled(false);
		scene27.enabled(false);

	} else {
		scene24.enabled(true);
		scene25.enabled(true);
		scene26.enabled(true);
		scene27.enabled(true);
	}

}

scrollmagic();
export default scrollmagic;

$(window).resize(sizeAll);
sizeAll();

console.log = function() {};