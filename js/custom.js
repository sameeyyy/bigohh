 /*jshint esversion: 6*/



//$(document).getElementsByClassName("navani2").addEventListener("click", test);
//var el = document.querySelector("body");
//
//
//function test() {
//	"use strict";
//	el.setAttribute('data-stating', 'who');
//}




 // ************************ PARALLAX ELEMENT **********************
 (function () {
 	"use strict";
 	// Add event listener
 	document.addEventListener("mousemove", parallax);
 	const elem = document.querySelector("#parallax");
 	// Magic happens here
 	function parallax(e) {
 		let _w = window.innerWidth / 2;
 		let _h = window.innerHeight / 2;
 		let _mouseX = e.clientX;
 		let _mouseY = e.clientY;
 		let _depth1 = `${50 - (_mouseX - _w) * 0.002}% ${50 - (_mouseY - _h) * 0.002}%`;
 		let x = `${_depth1}`;
 		elem.style.backgroundPosition = x;
 	}

 })();

 // var lFollowX = 0,
 //     lFollowY = 0,
 //     x = 0,
 //     y = 0,
 //     friction = 1 / 30;

 // function animate() {
 //   x += (lFollowX - x) * friction;
 //   y += (lFollowY - y) * friction;

 //   translate = 'translate(' + x + 'px, ' + y + 'px) scale(1.1)';

 //   $('#project-button, #project-button2, #project-button3, #project-button4').css({
 //     '-webit-transform': translate,
 //     '-moz-transform': translate,
 //     'transform': translate
 //   });

 //   window.requestAnimationFrame(animate);
 // }

 // $(window).on('mousemove click', function(e) {

 //   var lMouseX = Math.max(-100, Math.min(100, $(window).width() / 2 - e.clientX));
 //   var lMouseY = Math.max(-100, Math.min(100, $(window).height() / 2 - e.clientY));
 //   lFollowX = (80 * lMouseX) / 100; // 100 : 12 = lMouxeX : lFollow
 //   lFollowY = (60 * lMouseY) / 100;

 // });

 // animate();




 // ************************** SCROLL TO TOP **************************

 var scrollTop = $(".scrollTop");


 $(scrollTop).click(function () {
 	"use strict";
 	$('html, body').animate({
 		scrollTop: 0
 	}, 1000); // 800 is default
 	return false;

 });


 // ************************** MOBILE NAV **************************


//  $('.menu-icon').on('click', function(){
// 	$('.bar').toggleClass('bar--active')
// 	$('.overlay').toggleClass('overlay--active') 
//   })
	
//   $('.overlay').on('click', function(){
// 	if($('.overlay').hasClass('overlay--active')){
// 	  $('.overlay').removeClass('overlay--active')
// 	  $('.bar').removeClass('bar--active')
// 	}
//   })



 function openNav() {
 	"use strict";
 	document.getElementById("nc1").style.transition = "3s";
 	document.getElementById("myNav").style.width = "100%";
 	document.getElementById("myNav").style.height = "100%";
 	document.getElementById("nc1").style.opacity = "1";
 	document.getElementById("myNav").style.opacity = "1";
 	document.getElementById("myNav").style.pointerEvents = "initial";
 }

 function closeNav() {
 	"use strict";
 	document.getElementById("nc1").style.transition = "0.5s";
 	document.getElementById("myNav").style.transition = "0.5s";
 	document.getElementById("nc1").style.opacity = "0";
 	document.getElementById("myNav").style.width = "80%";
 	document.getElementById("myNav").style.height = "80%";
 	document.getElementById("myNav").style.opacity = "0";
 	document.getElementById("myNav").style.pointerEvents = "none";
 }

$('#nav-icon2').click(openNav)
$('#nav-icon2').click(function(){
$(this).toggleClass('open');
if($(this).hasClass('open')) {
	$('#nav-icon2').click(closeNav);
}
else {
	$('#nav-icon2').click(openNav);
}
})

//  $(document).ready(function(){
// 	$('#nav-icon2').click(openNav)
// 	$('#nav-icon2').click(function(){
// 		$(this).toggleClass('open');
// 	});
// 	$('#opened').click(closeNav);
// });

 // ************************** CONTACT OVERLAY **************************

 function openCont() {
 	"use strict";
 	document.getElementById("oc2").style.transition = "3s";
 	document.getElementById("contact-overlay").style.height = "95%";
 	document.getElementById("oc2").style.opacity = "1";
 }

 function closeCont() {
 	"use strict";
 	document.getElementById("oc2").style.transition = "0.5s";
 	document.getElementById("contact-overlay").style.transition = "0.5s";
 	document.getElementById("oc2").style.opacity = "0";
 	document.getElementById("contact-overlay").style.height = "0%";
 }

 // ************************** FOOTER YEAR **************************

 var dteNow = new Date();
 var intYear = dteNow.getFullYear();

 $('.cr-text').each(function () {
 	"use strict";
 	var text = $(this).text();
 	$(this).text(text.replace('CurrentYear', intYear));
 });



 /************************ WORK ANIMATION *************************/

//  if ($("body").data("state") === "work") {
	function viIn() {
		"use strict";
		$('.vi-work').addClass('show');
	}
   
	function viOut() {
		"use strict";
		$('.vi-work').removeClass('show');
	}
   
	$('.vi').hover(viIn, viOut);
   
	/* FOR V2 */
   
	function awIn() {
		"use strict";
		$('.aw-work').addClass('show');
	}
   
	function awOut() {
		"use strict";
		$('.aw-work').removeClass('show');
	}
   
	$('.aw').hover(awIn, awOut);
// }

/************************ COUNTUP ANIMATION *************************/

if ($("body").data("state") === "what") {
 var a = 0;
 $(window).scroll(function () {
 	"use strict";
 	var oTop = $('#counter').offset().top - window.innerHeight;
 	if (a === 0 && $(window).scrollTop() > oTop) {
 		$('.counter-value').each(function () {
 			var $this = $(this),
 				countTo = $this.attr('data-count');
 			$({
 				countNum: $this.text()
 			}).animate({
 					countNum: countTo
 				},

 				{

 					duration: 3000,
 					easing: 'swing',
 					step: function () {
 						$this.text(Math.floor(this.countNum));
 					},
 					complete: function () {
 						$this.text(this.countNum);
 						//alert('finished');
 					}

 				});
 		});
 		a = 1;
 	}

 });
}

 /************************ PRELOADER *************************/

// window.addEventListener('load', () => {
//	 "use strict";
//    setTimeout(function() {
//        const preload = document.querySelector('.preload');
//        preload.classList.add('preload-finish');
//      }, 2000);
//});

 /************************** CURSOR **************************/

// document.getElementsByTagName("body")[0].addEventListener("mousemove", function(n) {
// 	t.style.left = n.clientX + "px", 
// 	t.style.top = n.clientY + "px", 
// 	e.style.left = n.clientX + "px", 
// 	e.style.top = n.clientY + "px", 
// 	i.style.left = n.clientX + "px", 
// 	i.style.top = n.clientY + "px"
// });
// var t = document.getElementById("cursor"),
// 	e = document.getElementById("cursor2"),
// 	i = document.getElementById("cursor3");
// function n(t) {
// 	e.classList.add("hover"), i.classList.add("hover")
// }
// function s(t) {
// 	e.classList.remove("hover"), i.classList.remove("hover")
// }
// s();
// for (var r = document.querySelectorAll(".hover-target"), a = r.length - 1; a >= 0; a--) {
// 	o(r[a])
// }
// function o(t) {
// 	t.addEventListener("mouseover", n), t.addEventListener("mouseout", s)
// }


//  console.log = function() {}




// function load(files) {
//     // var src = document.createElement("script");
//     // src.setAttribute("type", "text/javascript");
//     // src.setAttribute("src", "file");
//     // document.getElementsByTagName("head")[0].appendChild(src);

//     $js.http.get(files[0], {}, function(data) {
//       eval(data);
      
//       if (files.length > 1) {
//         load(files.slice(1));
//       }
//     });
//   }

//   var dependencies = [
//     "/scrollmagic.js",
//     "/smoothscroll.js"
//   ];

//   load(dependencies);